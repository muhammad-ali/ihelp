class ChatMessagesController < ApplicationController
	
	def create
	  @chat_message = ChatMessage.new(chat_params)
	  respond_to { |format| format.js }
	end

	def index
		@chat_message = ChatMessage.new		
	end

	
	
	private
	
	def chat_params
		params.require(:chat_message).permit(:name, :message)
	end

end
